let express = require('express');
let socket = require('socket.io');

let app = express();
let server = app.listen(4200);
let io = socket(server);

app.use(express.static('../public'));

console.log('Server running..');

io.sockets.on('connection', (socket) => {
    console.log('new connection: ' + socket.id);

    socket.on('mouse', (data) => {
        socket.broadcast.emit('mouse', data);
    });
});