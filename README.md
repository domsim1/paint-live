# Paint Live
Paint live is a canvas that everyone can draw on together in real-time.  
Everyone starts with a blank canvas that connects to a socket.io server used to send and receive brushstrokes from all other connections.

# Set-up
run: "npm install"  
set socket in "/public/sketch.js" to server address. (localhost is default)  
optional: server port can be changed in "/src/server.js". (4200 is default)  
run: "node server.js" in the src directory to start the server.

# How-to
Once you are on the webpage just start drawing on the canvas with the left mouse button!  