let socket;

function setup() {
    let canvas = createCanvas(640, 480);
    background(250);
    canvas.parent('sketch-holder');

    socket = io.connect('http://localhost:4200');

    brushColor = [random(0, 255), random(0, 255), random(0, 255)];
    brushSize = random(5, 60);

    redSlider = createSlider(0, 255, brushColor[0]);
    greenSlider = createSlider(0, 255, brushColor[1]);
    blueSlider = createSlider(0, 255, brushColor[2]);
    redSlider.parent('colorred');
    greenSlider.parent('colorgreen');
    blueSlider.parent('colorblue');

    sizeSlider = createSlider(0, 60, brushSize);
    sizeSlider.parent('brushsize');

    socket.on('mouse', (data) => {
        noStroke();
        fill(data.color);
        ellipse(data.x, data.y, data.size, data.size);
    });


}

function mouseDragged() {

    brushColor = [redSlider.value(), greenSlider.value(), blueSlider.value()];
    brushSize = sizeSlider.value();

    let data = {
        x: mouseX,
        y: mouseY,
        size: brushSize,
        color: brushColor
    };

    socket.emit('mouse', data);

    noStroke();
    fill(brushColor);
    ellipse(mouseX, mouseY, brushSize, brushSize);
}

function draw() {
    if (mouseIsPressed) {
        mouseDragged();
    }
}

function onSave() {

}